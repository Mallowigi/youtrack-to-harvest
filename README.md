# Youtrack to Harvest Extension

Currently based on version 2.04 in the 
[Chrome Store](https://chrome.google.com/webstore/detail/harvest-time-tracker/fbpiglieekigmkeebmeohkelfpjjlaia)

## Additional features compared to the official version:

* Track time in [JetBrains YouTrack](http://www.jetbrains.com/youtrack/). Timer icon appears on the 
  full issue view, and on the popup in the agile view when you double-click an issue card.

## Installation

1. Go to [GitLab](http://git.codeoasis.com/eliorb/youtrack-to-harvest) in Gitlab.
1. Git clone the project
1. Then in Chrome go to [chrome://extensions](chrome://extensions).
1. Tick "Developer mode" at the top.
1. Then click on the button "load unpaqueted extension" and select the folder.
1. Open an issue in Youtrack: There is a timer that allows you to set your harvest tracker on the current task. 
1. Enjoy :)

